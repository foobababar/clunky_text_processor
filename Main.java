public class Main {
  public static void main(String[] args){
    if(args.length != 2){
      System.out.println("Expected exactly 2 arguments but got : "+args.length);
      System.exit(1);
    }

    Processor proc = new Processor();
    proc.loadTextFile(args[0]);

    //proc.showLines();

    proc.process();
    
    proc.export(args[1]);
  }
}
