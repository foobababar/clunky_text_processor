import java.io.FileNotFoundException;
import java.util.Scanner;

import java.io.IOException;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;


public class Processor {
  ArrayList<String> lst;
  ArrayList<String> types;
  ArrayList<String> output;

  public Processor(){
    this.lst=new ArrayList<>();
    this.types=new ArrayList<>();
    this.output=new ArrayList<>();
  }

  public String header(){
    return """ 
      <!DOCTYPE html><html><head><style>
      ul {
        background-color: #b9edc5;
      }
      
      h2 {
        text-decoration: underline;
      }

      .code {
        background-color: lightblue; 
        right: 40px;
        font-family: monospace;
        padding: 10px;
      }
      .code br {
        display: none;
      }
      
      .text {
        background-color: rgb(255, 229, 204); 
        padding: 10px;
      }
      
      .todo {
        background-color: red;
      }

      </style></head><body>
      """;
  }

  public String footer(){
    return "</body></html>";
  }

  public void loadTextFile(String filename){
    File path = new File(filename); //no need to close 

    try (Scanner myReader = new Scanner(path)){

      while (myReader.hasNextLine()) {
        //first pass
        String data = myReader.nextLine(); //data contains 1 line of text
        if(data.length() == 0){ //handle empty lines
          this.lst.add("");
          this.types.add("EMPTY");
          continue;
        }
         
        this.lst.add(data);

        if(data.equals("<c>")){
          this.types.add("<CODE>");
        }
        else if(data.equals("<t>")){
          this.types.add("<TEXT>");
        }
        else if(data.equals("<l>")){
          this.types.add("<LIST>");
        }
        else if(data.equals("TODO")){
          this.types.add("TODO");
        }
        else if(data.equals("<img>")){
          this.types.add("<IMG>");
        }

        else { //title or text
          int i = 0;
          if(data.charAt(i)=='#'){
            while(data.charAt(i)=='#') i++;
            this.types.add("<TITLE>"+i);
          }
          else { //text
            this.types.add("TEXT");
          }
        }
      }//<- end of while loop
    } catch (FileNotFoundException e) {  e.printStackTrace();  }
  }

  public void export(String filename){
    File file = new File(filename);

    try (FileWriter f = new FileWriter(file)){
      f.write(this.header()+"\n");

      for(int i=0; i<this.output.size(); i++){
        f.write(this.output.get(i)+"\n");
      }
      f.write(this.footer());
    }
    catch(IOException e){   e.printStackTrace();   }
  }


  public void process(){
    int i=0;
    while(i<this.types.size()){

      if(this.types.get(i).equals("EMPTY")){
        i++;
      }

      else if(this.types.get(i).regionMatches(0, "<TITLE>", 0, 7)){
        char headerNum = this.types.get(i).charAt(7);
        // `## title`  becomes  <h2>title</h2> 
        this.output.add("<h"+headerNum+">"+this.lst.get(i).substring(Character.getNumericValue(headerNum), this.lst.get(i).length())+"</h"+headerNum+">");
        i++;
      }

      else if(this.types.get(i).equals("<TEXT>")){
        this.output.add("<p class=\"text\">");
        i++; 
        
        while(i<this.types.size() && this.types.get(i).equals("TEXT")){
          this.output.add(this.lst.get(i)+"<br>");
          i++;
        }
        this.output.add("</p>");
      }


      else if(this.types.get(i).equals("<CODE>")){
        //warn if empty <l> at eof
        if(i == this.types.size()-1){
          System.out.println("WARNING : <c> tag at eof");
          this.output.add("<p></p>" );
          break; //exit while loop
        }


        this.output.add("<pre class=\"code\">");
        i++;

        if(! this.types.get(i).equals("TEXT")){
          this.output.add("<p><p>");  
          System.out.println("WARNING : <c> tag without body on line "+i);
          continue;
        }

        while(i<this.types.size() && this.types.get(i).equals("TEXT")){
          this.output.add(this.lst.get(i));
          i++;
        }
        this.output.add("</pre>");
      }

      else if(this.types.get(i).equals("<LIST>")){
        //warn if empty <l> at eof
        if(i == this.types.size()-1){
          System.out.println("WARNING : empty <l> tag at eof");
          this.output.add("<ul></ul>" );
          break; //exit while loop
        }

        this.output.add("<ul>");
        i++;

        //warn if next line is not text
        if(! this.types.get(i).equals("TEXT")){
          this.output.add("<ul></ul>");  
          System.out.println("WARNING : <l> tag without body on line "+i);
          continue;
        }

        while(i<this.types.size() && this.types.get(i).equals("TEXT")){
          this.output.add("<li>"+this.lst.get(i).substring(1, this.lst.get(i).length())+"</li>");
          i++;
        }

        this.output.add("</ul>");
      }

      else if(this.types.get(i).equals("TODO")){
        this.output.add("<p class=\"todo\">"+"TODO"+"</p>");
        i++;
      }

      else if(this.types.get(i).equals("<IMG>")){

        //warn if orphan <img> at eof 
        if(i == this.types.size()-1){
          System.out.println("WARNING : empty <img> tag at eof");
          this.output.add("<img src=\"\">" );
          break; //exit while loop
        }

        i++;

        //warn if next line is not text
        if(! this.types.get(i).equals("TEXT")){
          this.output.add("<img src=\"\">");  
          System.out.println("WARNING : <img> tag without body on line "+i);
          continue;
        }


        this.output.add("<img src=\""+this.lst.get(i)+"\">" );
        i++;
      }

      else if(this.types.get(i).equals("TEXT")){
        System.out.println("WARNING : Found line of text without header on line "+(i+1));
        this.output.add("<p>"+this.lst.get(i)+"</p>");
        i++;
      }
    }
  }
}
